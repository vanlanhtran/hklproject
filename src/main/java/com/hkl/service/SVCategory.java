package com.hkl.service;

import java.util.List;

import com.hkl.model.Category;

public interface SVCategory {

	List<Category> getAllCategory();
	void addCategory(Category category);
	void updateCategory(Category category);
	Category getCategoryById(int id);
	void deleteCategory(Category category);
	List<Category> searchCategory(String categoryName);
}
