package com.hkl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hkl.dao.DAOProduct;
import com.hkl.model.Product;

@Service
public class ImplSvProduct implements SVProduct {

	@Autowired
	private DAOProduct daoProductl;
	
	@Override
	@Transactional
	public List<Product> getAllProduct() {
		return daoProductl.getAllProduct();
	}

	@Override
	@Transactional
	public void addProduct(Product product) {
		daoProductl.addProduct(product);
	}

	@Override
	@Transactional
	public void updateProduct(Product product) {
		daoProductl.updateProduct(product);
	}

	@Override
	@Transactional
	public Product getProductById(int id) {
		return daoProductl.getProductById(id);
	}

	@Override
	@Transactional
	public void deleteProduct(Product product) {
		daoProductl.deleteProduct(product);
	}

	@Override
	public List<Product> getProductByPostId(int postId) {
		// TODO Auto-generated method stub
		return daoProductl.getProductByPostId(postId);
	}

	@Override
	public List<Product> getProductByFrontEnd(String searchText) {
		// TODO Auto-generated method stub
		return daoProductl.getProductByFrontEnd(searchText);
	}

}
