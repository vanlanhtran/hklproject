package com.hkl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hkl.dao.DAOCity;
import com.hkl.model.City;

@Service
public class ImplSvCity implements SVCity {

	@Autowired
	private DAOCity daoCity;
	
	@Override
	@Transactional
	public List<City> getAllCity() {
		return daoCity.getAllCity();
	}

	@Override
	@Transactional
	public void addCity(City city) {
		daoCity.addCity(city);
	}

	@Override
	@Transactional
	public void updateCity(City city) {
		daoCity.updateCity(city);
	}

	@Override
	@Transactional
	public City getCityById(int id) {
		return daoCity.getCityById(id);
	}

	@Override
	@Transactional
	public void deleteCity(City city) {
		daoCity.deleteCity(city);
	}

	@Override
	public List<City> searchCiry(String cityName) {
		// TODO Auto-generated method stub
		return daoCity.searchCiry(cityName);
	}

}
