package com.hkl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.hkl.dao.DAOUser;
import com.hkl.model.User;

@Service
public class ImplSvUser implements SVUser {

	@Autowired
	private DAOUser daoUser;
	
	@Override
	public List<User> getAllUser() {
		return daoUser.getAllUser();
	}

	@Override
	public void addUser(User user) {
		daoUser.addUser(user);
	}

	@Override
	public void updateUser(User user) {
		daoUser.updateUser(user);
	}

	@Override
	public User getUserById(int id) {
		return daoUser.getUserById(id);
	}

	@Override
	public void deleteUser(User user) {
		daoUser.deleteUser(user);
	}

	@Override
	public List<User> searchUser(String userName) {
		// TODO Auto-generated method stub
		return daoUser.searchUser(userName);
	}

	@Override
	public User userLogin(String name, String pass) {
		// TODO Auto-generated method stub
		return daoUser.userLogin(name, pass);
	}

}
