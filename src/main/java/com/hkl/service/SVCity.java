package com.hkl.service;

import java.util.List;

import com.hkl.model.City;

public interface SVCity {
	List<City> getAllCity();
	void addCity(City city);
	void updateCity(City city);
	City getCityById(int id);
	void deleteCity(City city);
	List<City> searchCiry(String cityName);
}
