package com.hkl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hkl.dao.DAOPost;
import com.hkl.model.Post;

@Service
public class ImplSvPost implements SVPost {

	@Autowired
	private DAOPost daoPost;
	
	@Override
	@Transactional
	public List<Post> getAllPost() {
		return daoPost.getAllPost();
	}

	@Override
	@Transactional
	public void addPost(Post post) {
		addPost(post);
	}

	@Override
	@Transactional
	public void updatePost(Post post) {
		daoPost.updatePost(post);
	}

	@Override
	@Transactional
	public Post getPostById(int id) {
		return getPostById(id);
	}

	@Override
	@Transactional
	public void deletePost(Post post) {
		daoPost.deletePost(post);
	}

	@Override
	public List<Post> searchPost(String post) {
		return daoPost.searchPost(post);
	}

}
