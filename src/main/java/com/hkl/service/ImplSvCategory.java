package com.hkl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hkl.dao.DAOCategory;
import com.hkl.model.Category;

@Service
public class ImplSvCategory implements SVCategory {

	@Autowired
	private DAOCategory daoCategory;
	
	@Override
	@Transactional
	public List<Category> getAllCategory() {
		return daoCategory.getAllCategory();
	}

	@Override
	@Transactional
	public void addCategory(Category category) {
		daoCategory.addCategory(category);
	}

	@Override
	@Transactional
	public void updateCategory(Category category) {
		daoCategory.updateCategory(category);
	}

	@Override
	@Transactional
	public Category getCategoryById(int id) {
		return daoCategory.getCategoryById(id);
	}

	@Override
	@Transactional
	public void deleteCategory(Category category) {
		daoCategory.deleteCategory(category);
	}

	@Override
	public List<Category> searchCategory(String categoryName) {
		return daoCategory.searchCategory(categoryName);
	}

}
