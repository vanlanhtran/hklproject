package com.hkl.vm;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import com.hkl.model.City;

import com.hkl.service.SVCity;

public class VMCrudCity {
	@Wire("#modalCity") // way to access UI component
	private Window win;
	
	private boolean readOnly;
	
	private Object vmList;
	
	private City city;
	
	@WireVariable
	private SVCity svCity;
	
	@AfterCompose
	public void setup(@ContextParam(ContextType.VIEW) Component view, 
			@ExecutionArgParam("cityRecord") City city
			,@ExecutionArgParam("mode") String info
			,@ExecutionArgParam("vmList") Object vmList) {
		Selectors.wireComponents(view, this, false);
		setVmList(vmList);
		setCity(city);
		svCity = (SVCity) SpringUtil.getBean("ServiceCity");
		if(info.equals("VIEW")) {
				setReadOnly(true);
		}
	}

	@Command("saveCity")
	public void savecity(){
		try {
			svCity.updateCity(city);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Executions.sendRedirect("list.zul");
		
		win.detach();
	}
	
	 @Command("closeThis")
	    public void closeThis() {
	        win.detach();
	    }
	 
	public Window getWin() {
		return win;
	}

	public void setWin(Window win) {
		this.win = win;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Object getVmList() {
		return vmList;
	}

	public void setVmList(Object vmList) {
		this.vmList = vmList;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	
}
