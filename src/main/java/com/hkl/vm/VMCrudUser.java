package com.hkl.vm;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import com.hkl.model.Category;
import com.hkl.model.User;
import com.hkl.service.SVCategory;
import com.hkl.service.SVUser;

public class VMCrudUser {
	@Wire("#modalUser") // way to access UI component
	private Window win;
	
	private boolean readOnly;
	
	private Object vmList;
	
	private User user;
	
	@WireVariable
	private SVUser svUser;
	
	@AfterCompose
	public void setup(@ContextParam(ContextType.VIEW) Component view, 
			@ExecutionArgParam("userRecord") User user
			,@ExecutionArgParam("mode") String info
			,@ExecutionArgParam("vmList") Object vmList) {
		Selectors.wireComponents(view, this, false);
		setVmList(vmList);
		setUser(user);
		svUser = (SVUser) SpringUtil.getBean("ServiceUser");
		if(info.equals("VIEW")) {
				setReadOnly(true);
		}
	}
	
	@Command("saveCustomer")
	public void saveCustomer() {
		try {
			svUser.updateUser(user);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Executions.sendRedirect("list.zul");
		
		win.detach();
	}
	
    @Command("closeThis")
    public void closeThis() {
        win.detach();
    }
    
    
	public Window getWin() {
		return win;
	}

	public void setWin(Window win) {
		this.win = win;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Object getVmList() {
		return vmList;
	}

	public void setVmList(Object vmList) {
		this.vmList = vmList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
