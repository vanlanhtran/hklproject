package com.hkl.vm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;


import com.hkl.model.Category;
import com.hkl.service.SVCategory;

public class VMListCategory {

	@WireVariable
	private SVCategory svCategory;
	
	private Category selectedCategory;
	
	private String searchText;
	
	private List<Category> allCategory = null;
	
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) 
	{
		Selectors.wireComponents(view, this, false);
		svCategory = (SVCategory) SpringUtil.getBean("ServiceCategory");
		allCategory = svCategory.getAllCategory();
	}
	
	@Command("search")
	public void searchCategory() {
		if(svCategory.searchCategory(searchText) != null) {
			setAllCategory(svCategory.searchCategory(searchText));
		}else {
			setAllCategory(null);
			Clients.showNotification("Không tìm thấy loại hàng!", "info", null, "top_center", 3000);
		}
		BindUtils.postNotifyChange(null, null, VMListCategory.this, "allCategory");
	}
	
	@Command("onAddNew")
	public void addNewCategory(@BindingParam("vmList") Object vmList) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("mode", "ADD");
		map.put("vmList", vmList);
		map.put("categoryRecord", new Category());
		Executions.createComponents("add_view.zul", null,map);
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("openAsRead") // get selected obj by param
	public void readClass(@BindingParam("categoryRecord") Category cate,
			@BindingParam("vmList") Object vmList) {
		Map map = new HashMap();
		map.put("categoryRecord", cate);
		map.put("mode", "VIEW");
		map.put("vmList", vmList);
		Executions.createComponents("add_view.zul", null,map);
	}
	
	@Command("onEdit")
	public void editClass(@BindingParam("categoryRecord") Category cate,
			@BindingParam("vmList") Object vmList) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("mode", "EDIT");
		map.put("categoryRecord", cate);
		map.put("vmList", vmList);
		Executions.createComponents("add_view.zul", null,map);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("onDelete")
	public void deleteClass(@BindingParam("categoryRecord") Category cate) {
		String str = "Loại hàng " + cate.getCategoryname() + " sẽ bị xóa?";
		Messagebox.show(str, "Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (Messagebox.ON_OK.equals(event.getName())) {
					svCategory.deleteCategory(cate);
					Clients.showNotification("Xóa thành công!", "info", null, "top_center", 2000);					
					allCategory.remove(allCategory.indexOf(cate));
					BindUtils.postNotifyChange(null, null, VMListCategory.this, "allCategory");
				}
			}
		});
	}
	
	
	public Category getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(Category selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public List<Category> getAllCategory() {
		return allCategory;
	}

	public void setAllCategory(List<Category> allCategory) {
		this.allCategory = allCategory;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	
	
}
