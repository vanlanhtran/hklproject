package com.hkl.vm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;

import com.hkl.model.Category;
import com.hkl.model.User;
import com.hkl.service.SVCategory;
import com.hkl.service.SVUser;

public class VMListUser {
	
	@WireVariable
	private SVUser svUser;
	
	private User selectedUser;
	
	private String searchText;
	
	private List<User> allUser = null;

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) 
	{
		Selectors.wireComponents(view, this, false);
		svUser = (SVUser) SpringUtil.getBean("ServiceUser");
		allUser = svUser.getAllUser();
	}
	
	@Command("search")
	public void searchCategory() {
		if(svUser.searchUser(searchText) != null) {
			setAllUser(svUser.searchUser(searchText));
		}else {
			setAllUser(null);
			Clients.showNotification("Không tìm thấy user!", "info", null, "top_center", 3000);
		}
		BindUtils.postNotifyChange(null, null, VMListUser.this, "allUser");
	}
	
	@Command("onAddNew")
	public void addNewCategory(@BindingParam("vmList") Object vmList) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("mode", "ADD");
		map.put("vmList", vmList);
		map.put("userRecord", new User());
		Executions.createComponents("add_view.zul", null,map);
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("openAsRead") // get selected obj by param
	public void readUser(@BindingParam("userRecord") User user,
			@BindingParam("vmList") Object vmList) {
		Map map = new HashMap();
		map.put("userRecord", user);
		map.put("mode", "VIEW");
		map.put("vmList", vmList);
		Executions.createComponents("add_view.zul", null,map);
	}
	
//	@Command("onEdit")
//	public void editClass(@BindingParam("userRecord") User user,
//			@BindingParam("vmList") Object vmList) {
//		final HashMap<String, Object> map = new HashMap<String, Object>();
//		map.put("mode", "EDIT");
//		map.put("userRecord", user);
//		map.put("vmList", vmList);
//		Executions.createComponents("add_view.zul", null,map);
//	}
//	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("onDelete")
	public void deleteUser(@BindingParam("userRecord") User user) {
		String str = "Người dùng " + user.getFullName() + " sẽ bị xóa?";
		Messagebox.show(str, "Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (Messagebox.ON_OK.equals(event.getName())) {
					svUser.deleteUser(user);
					Clients.showNotification("Xóa thành công!", "info", null, "top_center", 2000);					
					allUser.remove(allUser.indexOf(user));
					BindUtils.postNotifyChange(null, null, VMListUser.this, "allUser");
				}
			}
		});
	}
	
	
	
	
	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public List<User> getAllUser() {
		return allUser;
	}

	public void setAllUser(List<User> allUser) {
		this.allUser = allUser;
	}
	
	
}
