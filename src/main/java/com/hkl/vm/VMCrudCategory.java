package com.hkl.vm;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import com.hkl.model.Category;
import com.hkl.service.SVCategory;



public class VMCrudCategory {

	@Wire("#modalCategory") // way to access UI component
	private Window win;
	
	private boolean readOnly;
	
	private Object vmList;
	
	private Category category;
	
	@WireVariable
	private SVCategory svCategory;
	
	@AfterCompose
	public void setup(@ContextParam(ContextType.VIEW) Component view, 
			@ExecutionArgParam("categoryRecord") Category category
			,@ExecutionArgParam("mode") String info
			,@ExecutionArgParam("vmList") Object vmList) {
		Selectors.wireComponents(view, this, false);
		setVmList(vmList);
		setCategory(category);
		svCategory = (SVCategory) SpringUtil.getBean("ServiceCategory");
		if(info.equals("VIEW")) {
				setReadOnly(true);
		}
	}
	
	@Command("saveCategory")
	public void saveCustomer() {
		try {
			svCategory.updateCategory(category);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Executions.sendRedirect("list.zul");
		
		win.detach();
	}
	
    @Command("closeThis")
    public void closeThis() {
        win.detach();
    }

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Object getVmList() {
		return vmList;
	}

	public void setVmList(Object vmList) {
		this.vmList = vmList;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
    
    
}
