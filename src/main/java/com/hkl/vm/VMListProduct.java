package com.hkl.vm;

import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import com.hkl.model.Post;
import com.hkl.model.Product;
import com.hkl.model.User;
import com.hkl.service.SVProduct;
import com.hkl.service.SVUser;

public class VMListProduct {
	@Wire("#modalProduct") // way to access UI component
	private Window win;
		
	private Object vmList;
	
	private List<Product> allProduct;
	
	private Product selectedProduct;
	
	@WireVariable
	private SVProduct svProduct;
	
	@AfterCompose
	public void setup(@ContextParam(ContextType.VIEW) Component view, 
			@ExecutionArgParam("postRecord") Post post
			,@ExecutionArgParam("postId") int postId
			,@ExecutionArgParam("mode") String info
			,@ExecutionArgParam("vmList") Object vmList) {
		Selectors.wireComponents(view, this, false);
		setVmList(vmList);
		svProduct = (SVProduct) SpringUtil.getBean("ServiceProduct");

		allProduct = svProduct.getProductByPostId(postId);
	}
	
	@Command("closeThis")
    public void closeThis() {
        win.detach();
    }

	public Window getWin() {
		return win;
	}

	public void setWin(Window win) {
		this.win = win;
	}

	public Object getVmList() {
		return vmList;
	}

	public void setVmList(Object vmList) {
		this.vmList = vmList;
	}



	public List<Product> getAllProduct() {
		return allProduct;
	}

	public void setAllProduct(List<Product> allProduct) {
		this.allProduct = allProduct;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}
	
	
}
