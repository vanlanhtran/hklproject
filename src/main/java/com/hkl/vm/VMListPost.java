package com.hkl.vm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;

import com.hkl.model.Post;
import com.hkl.model.User;
import com.hkl.service.SVPost;


public class VMListPost {

	@WireVariable
	private SVPost svPost;
	
	private Post selectedPost;
	
	private String searchText;
	
	private List<Post> allPost = null;
	
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) 
	{
		Selectors.wireComponents(view, this, false);
		svPost = (SVPost) SpringUtil.getBean("ServicePost");
		allPost = svPost.getAllPost();
	}
	
	@Command("search")
	public void searchCategory() {
		if(svPost.searchPost(searchText) != null) {
			setAllPost(svPost.searchPost(searchText));
		}else {
			setAllPost(null);
			Clients.showNotification("Không tìm thấy bài đăng!", "info", null, "top_center", 3000);
		}
		BindUtils.postNotifyChange(null, null, VMListPost.this, "allPost");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("onDelete")
	public void deleteUser(@BindingParam("postRecord") Post post) {
		String str = "Bài đăng của user: " + post.getUser().getFullName() + " sẽ bị xóa?";
		Messagebox.show(str, "Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (Messagebox.ON_OK.equals(event.getName())) {
					svPost.deletePost(post);
					Clients.showNotification("Xóa thành công!", "info", null, "top_center", 2000);					
					allPost.remove(allPost.indexOf(post));
					BindUtils.postNotifyChange(null, null, VMListPost.this, "allPost");
				}
			}
		});
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("openAsRead") // get selected obj by param
	public void readUser(@BindingParam("postRecord") Post post, @BindingParam("postId") int postId,
			@BindingParam("vmList") Object vmList) {
		Map map = new HashMap();
		map.put("userRecord", post);
		map.put("postId", postId);
		map.put("mode", "VIEW");
		map.put("vmList", vmList);
		Executions.createComponents("list_product.zul", null,map);
	}
	
	
	public Post getSelectedPost() {
		return selectedPost;
	}

	public void setSelectedPost(Post selectedPost) {
		this.selectedPost = selectedPost;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public List<Post> getAllPost() {
		return allPost;
	}

	public void setAllPost(List<Post> allPost) {
		this.allPost = allPost;
	}
	
	
	
}
