package com.hkl.vm;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.impl.ClientBinderCommandParser;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import com.hkl.model.User;
import com.hkl.service.SVUser;

public class VMLogin {
	
	@Wire("#modalLogin") // way to access UI component
	private Window win;
	
	private String userName;
	
	private String password;
	
	@WireVariable
	private SVUser svUser;
	
	@AfterCompose
	public void setup(@ContextParam(ContextType.VIEW) Component view) {	
		Selectors.wireComponents(view, this, false);
		svUser = (SVUser) SpringUtil.getBean("ServiceUser");
		
	}
	
	@Command("login")
	public void login() {
		if(svUser.userLogin(userName, password) != null) {
			User user = svUser.userLogin(userName, password);
			Clients.showNotification("Đăng nhập thành công: " +user.getFullName(), "info", null, "top_center", 4000);
		}else {
			Clients.showNotification("Đăng nhập không thành công!" , "info", null, "top_center", 4000);
		}
		
	}
	
	
	   @Command("closeThis")
	    public void closeThis() {
	        win.detach();
	        Executions.sendRedirect("index.zhtml");
	    }
	public Window getWin() {
		return win;
	}

	public void setWin(Window win) {
		this.win = win;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
