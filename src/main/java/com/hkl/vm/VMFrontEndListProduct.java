package com.hkl.vm;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;

import com.hkl.model.Category;
import com.hkl.model.City;
import com.hkl.model.Post;
import com.hkl.model.Product;
import com.hkl.service.SVCategory;
import com.hkl.service.SVCity;
import com.hkl.service.SVProduct;

public class VMFrontEndListProduct {
	
	private List<Product> allProduct;
	
	private List<Category> allCategory;
	
	private List<City> allCity;
	
	private Product selectedProduct;
	
	private String seletedCategory;
	
	private String searchText; 
	
	private City selectedCity;
	
	
	@WireVariable
	private SVProduct svProduct;
	
	@WireVariable
	private SVCategory svCategory;

	@WireVariable
	private SVCity svCity;
	
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		svProduct = (SVProduct) SpringUtil.getBean("ServiceProduct");
		svCategory = (SVCategory) SpringUtil.getBean("ServiceCategory");
		svCity = (SVCity) SpringUtil.getBean("ServiceCity");
		allProduct = svProduct.getAllProduct();
		allCategory = svCategory.getAllCategory();
		allCity = svCity.getAllCity();
		
	}
	
	@Command("search")
	public void search() {
		setAllProduct(svProduct.getProductByFrontEnd(searchText));
		BindUtils.postNotifyChange(null, null, VMFrontEndListProduct.this, "allProduct");
	}
	
	public List<Product> getAllProduct() {
		return allProduct;
	}

	public void setAllProduct(List<Product> allProduct) {
		this.allProduct = allProduct;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}
	public List<Category> getAllCategory() {
		return allCategory;
	}
	public void setAllCategory(List<Category> allCategory) {
		this.allCategory = allCategory;
	}
	public String getSeletedCategory() {
		return seletedCategory;
	}
	public void setSeletedCategory(String seletedCategory) {
		this.seletedCategory = seletedCategory;
	}

	public List<City> getAllCity() {
		return allCity;
	}
	public void setAllCity(List<City> allCity) {
		this.allCity = allCity;
	}
	public City getSelectedCity() {
		return selectedCity;
	}
	public void setSelectedCity(City selectedCity) {
		this.selectedCity = selectedCity;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	
	
	
}
