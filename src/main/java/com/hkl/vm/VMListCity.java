package com.hkl.vm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Messagebox;

import com.hkl.model.City;
import com.hkl.service.SVCity;

public class VMListCity {
	
	@WireVariable
	private SVCity svCity;
	
	private City selectedCity;
	
	private String searchText;
	
	private List<City> allCity = null;
	
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) 
	{
		Selectors.wireComponents(view, this, false);
		svCity = (SVCity) SpringUtil.getBean("ServiceCity");
		allCity = svCity.getAllCity();
	}
	
	@Command("search")
	public void searchCity() {
		if(svCity.searchCiry(searchText) != null) {
			setAllCity(svCity.searchCiry(searchText));;
		}else {
			setAllCity(null);
			Clients.showNotification("Không tìm thấy thành phố!", "info", null, "top_center", 3000);
		}
		BindUtils.postNotifyChange(null, null, VMListCity.this, "allCity");
	}
	
	@Command("onAddNew")
	public void addNewCity(@BindingParam("vmList") Object vmList) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("mode", "ADD");
		map.put("vmList", vmList);
		map.put("cityRecord", new City());
		Executions.createComponents("add_view.zul", null,map);
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("openAsRead") // get selected obj by param
	public void readClass(@BindingParam("cityRecord") City city,
			@BindingParam("vmList") Object vmList) {
		Map map = new HashMap();
		map.put("cityRecord", city);
		map.put("mode", "VIEW");
		map.put("vmList", vmList);
		Executions.createComponents("add_view.zul", null,map);
	}
	
	@Command("onEdit")
	public void editClass(@BindingParam("cityRecord") City city,
			@BindingParam("vmList") Object vmList) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("mode", "EDIT");
		map.put("cityRecord", city);
		map.put("vmList", vmList);
		Executions.createComponents("add_view.zul", null,map);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command("onDelete")
	public void deleteClass(@BindingParam("cityRecord") City city) {
		String str = "Thành phố " + city.getCityName() + " sẽ bị xóa?";
		Messagebox.show(str, "Xác nhận", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (Messagebox.ON_OK.equals(event.getName())) {
					svCity.deleteCity(city);
					Clients.showNotification("Xóa thành công!", "info", null, "top_center", 2000);					
					allCity.remove(allCity.indexOf(city));
					BindUtils.postNotifyChange(null, null, VMListCity.this, "allCity");
				}
			}
		});
	}

	public City getSelectedCity() {
		return selectedCity;
	}

	public void setSelectedCity(City selectedCity) {
		this.selectedCity = selectedCity;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public List<City> getAllCity() {

		return allCity;
	}

	public void setAllCity(List<City> allCity) {
		this.allCity = allCity;
	}
	
	
	
}
