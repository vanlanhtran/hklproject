package com.hkl.main;

import org.springframework.web.bind.annotation.RequestMapping;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

public class MyViewModel {

	private int count;

	@Init
	public void init() {
		count = 100;
	}

	@Command
	@NotifyChange("count")
	@RequestMapping("/a")
	public void cmd() {
		++count;
	}

	public int getCount() {
		return count;
	}
	@Command
	@RequestMapping("/abc")
	public void goCategory() {
		Executions.sendRedirect("/zul/category/list.zul");
	}
	
}
