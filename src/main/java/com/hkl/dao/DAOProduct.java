package com.hkl.dao;

import java.util.List;

import com.hkl.model.Product;

public interface DAOProduct {

	List<Product> getAllProduct();
	void addProduct(Product product);
	void updateProduct(Product product);
	Product getProductById(int id);
	void deleteProduct(Product product);
	List<Product> getProductByPostId(int postId);
	List<Product> getProductByFrontEnd(String searchText);
}
