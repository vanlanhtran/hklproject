package com.hkl.dao;

import java.util.List;

import com.hkl.model.Post;


public interface DAOPost {
	
	List<Post> getAllPost();
	void addPost(Post post);
	void updatePost(Post post);
	Post getPostById(int id);
	void deletePost(Post post);
	List<Post> searchPost(String post);
}
