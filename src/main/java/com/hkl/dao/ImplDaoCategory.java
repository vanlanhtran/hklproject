package com.hkl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hkl.model.Category;
import com.hkl.model.Product;

@Repository
public class ImplDaoCategory implements DAOCategory {

	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getAllCategory() {
		return getCurrentSession().createQuery("from Category").list();
	}

	@Override
	public void addCategory(Category category) {
		getCurrentSession().save(category);
	}

	@Override
	public void updateCategory(Category category) {
		getCurrentSession().saveOrUpdate(category);
	}

	@Override
	public Category getCategoryById(int id) {
		return (Category) getCurrentSession().get(Category.class, id);
	}
	
	// before delete --> set null at product.
	@SuppressWarnings("unchecked")
	@Override
	public void deleteCategory(Category category) {
		String hql = "from Product p where p.category = "+ category.getId();
		Query query = getCurrentSession().createQuery(hql);
		List<Product> listProduct = query.list();
		for(Product p : listProduct) {
			p.setCategory(null);
		}
		getCurrentSession().delete(category);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> searchCategory(String categoryName) {
		Query query = getCurrentSession().createQuery("from Category c where c.categoryname like ?");
		query.setString(0, "%"+categoryName+"%");
		List<Category> listCategory = query.list();
		if(!listCategory.isEmpty()) {
			return listCategory;
		}
		return null;
	}

}
