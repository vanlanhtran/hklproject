package com.hkl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hkl.model.Category;
import com.hkl.model.Post;
import com.hkl.model.User;
@Repository
public class ImplDaoUser implements DAOUser {

	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUser() {
		return getCurrentSession().createQuery("from User").list();
	}

	@Override
	public void addUser(User user) {
		getCurrentSession().save(user);
	}

	@Override
	public void updateUser(User user) {
		getCurrentSession().saveOrUpdate(user);
	}

	@Override
	public User getUserById(int id) {
		return (User) getCurrentSession().get(User.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteUser(User user) {
		String hql = "from Post p where p.user = "+ user.getId();
		Query query = getCurrentSession().createQuery(hql);
		List<Post> listPost = query.list();
		for(Post p : listPost) {
			p.setUser(null);
		}
		getCurrentSession().delete(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchUser(String userName) {
		Query query = getCurrentSession().createQuery("from User u where u.fullName like ?");
		query.setString(0, "%"+userName+"%");
		List<User> listUser= query.list();
		if(!listUser.isEmpty()) {
			return listUser;
		}
		return null;
	}

	@Override
	public User userLogin(String name, String pass) {
		Query query = getCurrentSession().createQuery("from User u where u.userName = ? and password = ?");
		query.setString(0, "%"+name+"%");
		query.setString(1, "%"+pass+"%");
		User listUser = (User) query.uniqueResult();
		
		if(listUser != null) {
			return listUser;
		}
		return null;
	}

}
