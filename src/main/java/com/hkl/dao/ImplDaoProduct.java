package com.hkl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hkl.model.Product;
@Repository
public class ImplDaoProduct implements DAOProduct {

	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllProduct() {
		return getCurrentSession().createQuery("from Product").list();
	}

	@Override
	public void addProduct(Product product) {
		getCurrentSession().save(product);
	}

	@Override
	public void updateProduct(Product product) {
		getCurrentSession().update(product);
	}

	@Override
	public Product getProductById(int id) {
		return (Product) getCurrentSession().get(Product.class, id);
	}

	@Override
	public void deleteProduct(Product product) {
		getCurrentSession().delete(product);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProductByPostId(int postId) {
		String hql = "from Product p where p.post = "+ postId;
		Query query = getCurrentSession().createQuery(hql);
		List<Product> listProduct = query.list();
		if(!listProduct.isEmpty()) {
			return listProduct;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProductByFrontEnd(String searchText) {


			Query query = getCurrentSession().createQuery("from Product p where p.productName like ? ");
			query.setString(0, "%"+searchText+"%");
			List<Product> listProduct = query.list();
			if(!listProduct.isEmpty()) {
				return listProduct;
			}
		
	
		
		
		
		return null;
	}

}
