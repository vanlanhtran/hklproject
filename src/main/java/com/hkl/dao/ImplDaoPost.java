package com.hkl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hkl.model.Post;
import com.hkl.model.Product;
import com.hkl.model.User;
@Repository
public class ImplDaoPost implements DAOPost {

	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Post> getAllPost() {
		return getCurrentSession().createQuery("from Post").list();
	}

	@Override
	public void addPost(Post post) {
		getCurrentSession().save(post);
	}

	@Override
	public void updatePost(Post post) {
		getCurrentSession().update(post);
	}

	@Override
	public Post getPostById(int id) {
		return (Post) getCurrentSession().get(Post.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deletePost(Post post) {
		String hql = "from Product p where p.post = "+ post.getId();
		Query query = getCurrentSession().createQuery(hql);
		List<Product> listProduct = query.list();
		for(Product p : listProduct) {
			p.setPost(null);
		}
		getCurrentSession().delete(post);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> searchPost(String post) {
		Query query = getCurrentSession().createQuery("from Post p where p.user.fullName like ?");
		query.setString(0, "%"+post+"%");
		List<Post> listPost = query.list();
		if(!listPost.isEmpty()) {
			return listPost;
		}
		return null;
	}

}
