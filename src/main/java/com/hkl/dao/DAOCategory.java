package com.hkl.dao;

import java.util.List;

import com.hkl.model.Category;

public interface DAOCategory {
	
	List<Category> getAllCategory();
	void addCategory(Category category);
	void updateCategory(Category category);
	Category getCategoryById(int id);
	void deleteCategory(Category category);
	List<Category> searchCategory(String categoryName);
}
