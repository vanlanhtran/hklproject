package com.hkl.dao;

import java.util.List;


import com.hkl.model.User;

public interface DAOUser {
	
	List<User> getAllUser();
	void addUser(User user);
	void updateUser(User user);
	User getUserById(int id);
	void deleteUser(User user);
	List<User> searchUser(String userName);
	User userLogin(String name, String pass);
}
