package com.hkl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.hkl.model.City;
import com.hkl.model.Post;
@Repository
public class ImplDaoCity implements DAOCity {
	
	@Autowired
	SessionFactory sessionFactory;
	
	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<City> getAllCity() {
		return getCurrentSession().createQuery("from City").list();
	}

	@Override
	public void addCity(City city) {
		getCurrentSession().save(city);
	}

	@Override
	public void updateCity(City city) {
		getCurrentSession().saveOrUpdate(city);
	}

	@Override
	public City getCityById(int id) {
		return (City) getCurrentSession().get(City.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteCity(City city) {
		String hql = "from Post p where p.city = "+ city.getId();
		Query query = getCurrentSession().createQuery(hql);
		List<Post> listPost = query.list();
		for(Post p : listPost) {
			p.setCity(null);
		}
		getCurrentSession().delete(city);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> searchCiry(String cityName) {
		Query query = getCurrentSession().createQuery("from City c where c.cityName like ?");
		query.setString(0, "%"+cityName+"%");
		List<City> listCity = query.list();
		if(!listCity.isEmpty()) {
			for (City city : listCity) {
				System.out.println("Prammm " + city.getCityName());
			}
			return listCity;
		}
		return null;
	}

}
